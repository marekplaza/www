from http.server import BaseHTTPRequestHandler, HTTPServer
import socket

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        hostname = socket.gethostname()
        message = f"<html><b>Hello Apius!</b><br>from: {hostname}</html>"
        self.send_response(200)
        self.end_headers()
        self.wfile.write(message.encode())

if __name__ == '__main__':
    server_address = ('', 8888)
    httpd = HTTPServer(server_address, SimpleHTTPRequestHandler)
    print(f"Starting http server on port {server_address[1]}")
    httpd.serve_forever()


