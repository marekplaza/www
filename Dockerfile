# Use an official lightweight Python image.
# https://hub.docker.com/_/python
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the server script to the container
COPY app.py .

# Run the server when the container launches
CMD ["python", "app.py"]
